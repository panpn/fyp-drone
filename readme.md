# Project overview #
A program that allows the user to modify the thrust of the Crazyflie 2.0 drone with the Muse 2014 headset by concentrating. When the user is detected to be concentrating, the thrust of the drone will be increased, and vice versa. 

## Primary project components ##

![overview.JPG](https://bitbucket.org/repo/KykrX7/images/1548610591-overview.JPG)

**Band Power Collector**: Collects data streamed from MuseIO. Uses python 3

**Band Power Processor**: Extracts and scales features, Predict result using pre-loaded SVM model. Uses python 3

**Experimental Control Program**: Displays instruction for cue-based experiment, records and displays detected command (whether the user is concentrating or not). Uses python 2

**Command Feedback Program**: Displays the command that is sent to the drone for the user's knowledge. Uses python 3

**Drone Control Program**: Sends command to the drone to increase or decrease thrust. Uses python 3




## Installation instructions ## 

1. Install Anaconda from https://www.continuum.io/downloads
2. 2 Conda environments are needed: 1 with Python 3, and the other with python 2
3. In the Python 3 environment, install python-osc, pyzmq, pygame and cflib using pip (e.g. python -m pip install python-osc)
4. In the Python 2 environment, install expyriment v0.8.0 using pip (python -m pip install expyriment==0.8.0)