import pygame
import sys
import zmq
import config
from pygame.locals import *

'''
Displays the command that is sent to the drone for the user's knowledge
Uses pygame 1.9.3 and python 3.6 in 32 bit
'''

white = (255,255,255)
black = (0,0,0)
commands = ["Inattention detected, Down", "Concentration detected, Up", "Bad contact quality, Down"]

class Pane(object):
    def __init__(self):
        pygame.init()
        self.font = pygame.font.SysFont('Calibri', 30)
        pygame.display.set_caption('Drone Control Detector')
        self.screen = pygame.display.set_mode((1000,600), 0, 32)

    def addText(self, text):
        self.screen.fill((black))
        self.screen.blit(self.font.render(text, True, white), (350, 270))
        pygame.display.update()

def listenToPort():
    context = zmq.Context()
    sub_socket = context.socket(zmq.SUB)
    sub_socket.connect("tcp://localhost:%s" % config.processed_data_port)
    sub_socket.setsockopt_string(zmq.SUBSCRIBE, config.result_only_topic_id)
    return sub_socket

if __name__ == '__main__':
    sub_socket = listenToPort()
    Pan3 = Pane()
    Pan3.addText("Initialising..")
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit();
                sys.exit();
        string = sub_socket.recv_string()
        topic, messagedata = string.split(" ", 1)
        # move up
        result_command = int(messagedata)
        Pan3.addText(commands[result_command])