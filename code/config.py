raw_data_topic_id = u"10001"
raw_data_port = u'5556'
processed_data_topic_id = u"10002"
processed_data_port = u'5557'
result_only_topic_id = u"10003"

HORSESHOE_PATH = "/muse/elements/horseshoe"
LOW_FREQS_ABSOLUTE_PATH = "/muse/elements/low_freqs_absolute"
DELTA_ABSOLUTE_PATH = "/muse/elements/delta_absolute"
THETA_ABSOLUTE_PATH = "/muse/elements/theta_absolute"
GAMMA_ABSOLUTE_PATH = "/muse/elements/gamma_absolute"
BETA_ABSOLUTE_PATH = "/muse/elements/beta_absolute"
ALPHA_ABSOLUTE_PATH = "/muse/elements/alpha_absolute"


HORSESHOE = ["cq", HORSESHOE_PATH]
LOW_FREQS_ABSOLUTE = ["low_freq", LOW_FREQS_ABSOLUTE_PATH]
DELTA_ABSOLUTE = ["delta", DELTA_ABSOLUTE_PATH]
THETA_ABSOLUTE = ["theta", THETA_ABSOLUTE_PATH]
GAMMA_ABSOLUTE = ["gamma", GAMMA_ABSOLUTE_PATH]
BETA_ABSOLUTE = ["beta", BETA_ABSOLUTE_PATH]
ALPHA_ABSOLUTE = ["alpha", ALPHA_ABSOLUTE_PATH]

phase_order = ["inattention", "concentration", "bad_cq"]

inattention_id = phase_order.index("inattention")
concentration_id = phase_order.index("concentration")
bad_cq_id = phase_order.index("bad_cq")

TP10 = "TP10"
AF8 = "AF8"
AF7 = "AF7"
TP9 = "TP9"

electrodes_order = [TP9, AF7, AF8, TP10]
channels_order = [ALPHA_ABSOLUTE, BETA_ABSOLUTE, GAMMA_ABSOLUTE, THETA_ABSOLUTE,
                  DELTA_ABSOLUTE, LOW_FREQS_ABSOLUTE,
                  HORSESHOE]

channel_pathnames = [x[1] for x in channels_order]
channels_names =  [x[0] for x in channels_order]

num_of_channels = 4
num_of_elements_per_channel = len(channel_pathnames)
