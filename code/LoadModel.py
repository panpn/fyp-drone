import pickle
import numpy as np

'''
Used by BandPowerProcessor to load the model and scaler.
'''
filename_all = "all"

def open_model():
    model_directory = r'..\data\ml-models\\'
    with open(model_directory + filename_all, 'rb') as saved_model:
        return pickle.load(saved_model)

def open_scaler():
    scaler_directory = r'..\data\ml-models\\'
    with open(scaler_directory + "scaler_" + filename_all, 'rb') as saved_scaler:
        return pickle.load(saved_scaler)


#load appropriate model
loaded_clf = open_model()
#load appropriate scaler
loaded_scaler = open_scaler()


def predict(row):
    #rescale
    scaled_row = loaded_scaler.transform(np.array(row).reshape(1,-1))
    # print("new_row", scaled_row)
    return loaded_clf.predict(scaled_row)[0]
