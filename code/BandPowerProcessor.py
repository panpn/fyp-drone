import zmq, config, LoadModel, json, time

'''
Receives band power readings from BandPowerCollector
Extracts and scales features
Predict detected command using pre-loaded classifier
'''
context = zmq.Context()
sub_socket = context.socket(zmq.SUB)
sub_socket.connect("tcp://localhost:%s" % config.raw_data_port)
sub_socket.setsockopt_string(zmq.SUBSCRIBE, config.raw_data_topic_id)

pub_socket = context.socket(zmq.PUB)
pub_socket.bind("tcp://127.0.0.1:%s" % config.processed_data_port)

# channels and frequencies to be concerned about
electrodes_to_use = [config.AF7, config.AF8]
frequencies_to_use = [config.ALPHA_ABSOLUTE, config.BETA_ABSOLUTE, config.GAMMA_ABSOLUTE, config.THETA_ABSOLUTE]

repeat_counter = [0] * len(electrodes_to_use) * len(frequencies_to_use)
current_row = []
prev_row = []
raw_row = []


def update_repeat_counter(row, prev, counter):
    ind = 0
    for current, previous in zip(row, prev):
        if current == previous:
            counter[ind] += 1
        else:
            counter[ind] = 0
        ind += 1
    return counter


def extract_features(m_raw_row):
    features = []
    m_is_good_cq = True
    for electrode_to_use in electrodes_to_use:
        start_electrode_ind = config.electrodes_order.index(electrode_to_use) * config.num_of_elements_per_channel
        contact_quality_ind = start_electrode_ind + config.channels_order.index(config.HORSESHOE)
        # check contact quality of channel
        if not m_raw_row[contact_quality_ind] == 1:
            # do not use rows with bad contact quality
            print("bad contact quality")
            m_is_good_cq = False
        for frequency_to_use in frequencies_to_use:
            # retrieve the band power reading for the particular frequency and channel
            frequency_ind = config.channels_order.index(frequency_to_use)
            features.append(m_raw_row[start_electrode_ind + frequency_ind])
    return m_is_good_cq, features


vote = [0] * 3
count = 0

while True:
    # receive FFT band power data
    topic = sub_socket.recv_string()
    raw_row = sub_socket.recv_json()
    # extract appropriate features and determine if quality is good
    is_good_contact_quality, current_row = extract_features(raw_row)
    # check if values are repeated, as Muse has the habit of displaying repeated values
    # with good contact quality reading, when the contact quality is actually poor
    repeat_counter = update_repeat_counter(current_row, prev_row, repeat_counter)
    is_repeated = any(i > 10 for i in repeat_counter)
    #if contact quality is good, pass it to the classifier to predict the result
    if is_good_contact_quality and not is_repeated:
        result = LoadModel.predict(current_row)
    else:
        result = -1
    # publish FFT band power data and detected command
    data_to_send = raw_row
    data_to_send.append(result)
    print(data_to_send, time.time())
    pub_socket.send_string(config.processed_data_topic_id, zmq.SNDMORE)
    pub_socket.send_json(data_to_send)
    if count == 10:
        # if 10 detected commands have been received, publish the most commonly detected command to the drone
        max_value = max(vote)
        final_result = vote.index(max_value)
        pub_socket.send_string("%s %d" % (config.result_only_topic_id, final_result))
        vote = [0] * 3
        count = 0
    else:
        # increase the vote for the particular detected command
        vote[int(result)] += 1
        count += 1
    prev_row = current_row
    current_row = []
