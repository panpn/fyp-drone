import argparse, csv, time
import LoadModel, config
import zmq
# python 3 is needed
from pythonosc import dispatcher
from pythonosc import osc_server

'''
Listens and collects data streamed from MuseIO
Run this command to listen to MuseIO
muse-io --dsp --device Muse-989D --osc osc.udp://localhost:5000 --osc-timestamp

Python 3 and python OSC library is needed
'''

TP10 = "TP10"
AF8 = "AF8"
AF7 = "AF7"
TP9 = "TP9"

csv_writer = None

# "/muse/batt",]

output_row = []
prev_row = []

current_row = [0] * config.num_of_elements_per_channel * config.num_of_channels
is_good_contact_quality = True
zmq_socket = None


def eeg_handler(unused_addr, args, *data):
    # [TP9, Fp1, Fp2, TP10]
    global output_row, is_good_contact_quality, repeat_counter
    eeg_data = [float(i) for i in data]
    channel = args[0]
    # print(channel, list(data))
    base_channel_index = config.channel_pathnames.index(channel)
    indices_to_insert = [base_channel_index + i * config.num_of_elements_per_channel for i in
                         range(config.num_of_channels)]
    for i, eeg in zip(indices_to_insert, eeg_data):
        current_row[i] = eeg
    if channel == config.HORSESHOE_PATH:
        # send current row
        zmq_socket.send_string(config.raw_data_topic_id, zmq.SNDMORE)
        zmq_socket.send_json(current_row)
        print(current_row, time.time())


# print(args[0], list(data))


def start_muse_listener():
    global dispatcher
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip",
                        default="127.0.0.1",
                        help="The ip to listen on")
    parser.add_argument("--port",
                        type=int,
                        default=5000,
                        help="The port to listen on")
    args = parser.parse_args()
    LoadModel.open_model()
    dispatcher = dispatcher.Dispatcher()
    dispatcher.map("/debug", print)
    for channel_to_listen in config.channel_pathnames:
        dispatcher.map(channel_to_listen, eeg_handler, channel_to_listen)
        print(channel_to_listen, eeg_handler, channel_to_listen)
    server = osc_server.BlockingOSCUDPServer(
        (args.ip, args.port), dispatcher)
    print("Serving on {}".format(server.server_address))
    server.serve_forever()


def start_publisher():
    global zmq_socket
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUB)
    zmq_socket.bind("tcp://127.0.0.1:%s" % config.raw_data_port)


if __name__ == "__main__":
    start_publisher()
    start_muse_listener()
