import zmq, config, cflib, msvcrt, time
from cflib.crazyflie import Crazyflie
from threading import Thread

'''
Sends command to the drone to increase or decrease thrust
'''
class CrazyFlieControl:
    def __init__(self, link_uri):
        """ Initialize and run the example with the specified link_uri """

        self._cf = Crazyflie()

        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)

        self._cf.open_link(link_uri)

        print('Connecting to %s' % link_uri)

    def _connected(self, link_uri):
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded."""

        # Start a separate thread to do the motor test.
        # Do not hijack the calling thread!
        Thread(target=self._ramp_motors).start()

    def _connection_failed(self, link_uri, msg):
        """Callback when connection initial connection fails (i.e no Crazyflie
        at the specified address)"""
        print('Connection to %s failed: %s' % (link_uri, msg))

    def _connection_lost(self, link_uri, msg):
        """Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range)"""
        print('Connection to %s lost: %s' % (link_uri, msg))

    def _disconnected(self, link_uri):
        """Callback when the Crazyflie is disconnected (called in all cases)"""
        print('Disconnected from %s' % link_uri)

    def _ramp_motors(self):
        thrust_mult = 1
        thrust_step = 400
        thrust = 20000
        pitch = 0
        roll = 0
        yawrate = 0
        max_thrust = 40000
        min_thrust = 10001
        context = zmq.Context()
        sub_socket = context.socket(zmq.SUB)
        sub_socket.connect("tcp://localhost:%s" % config.processed_data_port)
        sub_socket.setsockopt_string(zmq.SUBSCRIBE, config.result_only_topic_id)

        # Unlock startup thrust protection
        self._cf.commander.send_setpoint(0, 0, 0, 0)
        while True:
            if msvcrt.kbhit():
                key = msvcrt.getch()
                if key == b'x':
                    print('exit')
                    self._cf.commander.send_setpoint(0, 0, 0, 0)
                    # Make sure that the last packet leaves before the link is closed
                    # since the message queue is not flushed before closing
                    time.sleep(0.1)
                    self._cf.close_link()
                    break
            else:
                #receive message from ProcessDataMuse
                string = sub_socket.recv_string()
                topic, messagedata = string.split(" ", 1)
                result_command = int(messagedata)
                # classify into appropriate field
                if result_command == config.concentration_id:
                    # the user is concentrating, increase thrust of drone in steps of 5
                    for _ in range(5):
                        if thrust < max_thrust:
                            thrust += thrust_step * thrust_mult
                        if thrust > max_thrust:
                            thrust = max_thrust
                        print('up', thrust, time.time())
                        self._cf.commander.send_setpoint(roll,pitch, yawrate, thrust)
                        time.sleep(0.1)
                elif result_command == config.inattention_id or result_command == config.bad_cq_id:
                    # inattention or bad contact quality, reduce thrust of drone in steps of 5
                    for _ in range(5):
                        if thrust > min_thrust:
                            thrust -= thrust_step * thrust_mult
                        if thrust < min_thrust:
                            thrust = min_thrust
                        print('down', thrust, time.time())
                        self._cf.commander.send_setpoint(roll,pitch, yawrate, thrust)
                        time.sleep(0.1)
                else:
                    print('unknown command: ', messagedata)

if __name__ == '__main__':
    # Initialize the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)
    # Scan for Crazyflies and use the first one found
    print('Scanning interfaces for Crazyflies...')
    available = cflib.crtp.scan_interfaces()
    print('Crazyflies found:')
    for i in available:
        print(i[0])

    if len(available) > 0:
        le = CrazyFlieControl(available[0][0])
    else:
        print('No Crazyflies found, cannot run example')
