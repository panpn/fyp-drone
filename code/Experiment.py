#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Displays stimulus to instruct user to concentrate, not concentrate (inattention) or relax (close eyes)
Stores the FFT band power data and the command detected in an Expyriment data file
Subscribes to data from LogDataMuse.py
Provides feedback on which mental command is detected
Uses Expyriment library and Python 2.7
'''

from expyriment import design, control, stimuli
import time, zmq, config, csv
from ctypes import *

'''
Defines header of stored Expyriment result file. It stores the phase and the detected command, as well as the
FFT band power readings from each channel
'''


def create_header():
    result = ["Trial", "Start_Time", "Current_Time", "Phase", "Detected_Command"]
    for electrode in config.electrodes_order:
        for channel in config.channels_order:
            result.append(electrode.lower() + "_" + channel[0])
    return result


header = create_header()

# Create and initialize an Experiment
exp = design.Experiment("Recording of FFT band powers over concentration and inattention")
control.defaults.window_mode = True
control.initialize(exp)

relax_time = 10000
conc_time = 15000
inattention_time = 10000

num_iterations = 10
iter_index = 0
trial_start_time = 0

tone = stimuli.Tone(1000)

concentration_ind = 1
inattention_ind = 0
relax_ind = 2
port = config.processed_data_port
topic_id = config.processed_data_topic_id

concentration_title = "Concentrate"
inattention_title = "Inattention"
concentration_detected = "Concentration detected"
inattention_detected = "Inattention detected"
poor_contact_quality = "Poor contact quality"
titles = [inattention_title, concentration_title, "Relax"]
texts = [inattention_detected, concentration_detected, poor_contact_quality]
phases = ["inattention", "concentrate", "relax"]

stimuli_list = [0, 0, 0]

context = zmq.Context()
socket = context.socket(zmq.SUB)


# Set up the trials and stimulus displayed in the experiment
# Experiment consists of 10 blocks (iterations) and 3 trials (inattention, concentration, relax) in each block.

def design_experiment():
    exp.data_variable_names = create_header()
    for block_iter in range(num_iterations):
        b = design.Block()
        t_inattention = design.Trial()
        t_inattention.set_factor("phase", "inattention")
        s_inattention_instruction = stimuli.TextScreen(heading="Inattention. Relax your mind ",
                                                       text="Press any key to continue")
        s_inattention = stimuli.TextScreen(heading=titles[inattention_ind],
                                           text="")
        t_inattention.add_stimulus(s_inattention_instruction)
        t_inattention.add_stimulus(s_inattention)
        b.add_trial(t_inattention)

        t_conc = design.Trial()
        t_conc.set_factor("phase", "concentrate")
        s_conc_instruction = stimuli.TextScreen(heading="Concentrate on the screen",
                                                text="Press any key to continue")
        s1_conc = stimuli.TextScreen(heading=titles[concentration_ind],
                                     text="")
        t_conc.add_stimulus(s_conc_instruction)
        t_conc.add_stimulus(s1_conc)
        b.add_trial(t_conc)

        t_relax = design.Trial()
        t_relax.set_factor("phase", "relax")
        s_relax_instruction = stimuli.TextScreen(heading="Rest period. Close your eyes",
                                                 text="Press any key to continue")
        s_relax = stimuli.TextScreen(heading=titles[relax_ind],
                                     text="")
        t_relax.add_stimulus(s_relax_instruction)
        t_relax.add_stimulus(s_relax)
        b.add_trial(t_relax)
        exp.add_block(b)


def evaluate():
    socket.recv_string()
    serialised_data = socket.recv_json()
    data = serialised_data
    # data = json.loads(serialised_data)
    print(data)
    detected_command = int(data[-1])
    band_data = data[:-1]
    current_time = int(round(time.time() * 1000))
    exp.data.add([iter_index, trial_start_time, current_time, trial.get_factor("phase"),
                  config.phase_order[detected_command]] + band_data)
    stimuli_list[detected_command].present()


design_experiment()
# Start Experiment
control.start(skip_ready_screen=True)

for block in exp.blocks:
    stimuli.TextScreen(heading="Iteration " + str(iter_index + 1), text="Press any key to continue").present()
    exp.keyboard.wait()
    for trial in block.trials:
        instruction = trial.stimuli[0]
        expt_stimulus = trial.stimuli[1]
        phase_time = 0
        print(trial.get_factor("phase"))
        # display instruction
        instruction.present()
        if trial.get_factor("phase") == "inattention":
            heading = "Inattention"
            phase_time = inattention_time
        elif trial.get_factor("phase") == "concentrate":
            heading = "Concentrate"
            phase_time = conc_time
        else:
            heading = "Relax"
            phase_time = relax_time
        # prepare stimulus displaying instructed command (inattention) as heading and detected command
        for detected_ind, detected_text in enumerate(texts):
            print(detected_ind)
            stimuli_list[detected_ind] = stimuli.TextScreen(heading=heading,
                                                            text=detected_text)
        # wait for key press
        exp.keyboard.wait()

        # start recording FFT band power data and detected command
        trial_start_time = int(round(time.time() * 1000))
        socket = context.socket(zmq.SUB)
        print("Collecting updates from muse..")
        socket.connect("tcp://localhost:%s" % port)
        socket.setsockopt_string(zmq.SUBSCRIBE, topic_id)
        socket.setsockopt(zmq.LINGER, 0)
        expt_stimulus.present()
        exp.clock.wait(phase_time, function=evaluate)
        socket.close()
        tone.play()
        trial_end_time = int(round(time.time() * 1000))
    iter_index += 1
